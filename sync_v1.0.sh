#!/bin/bash
mount_cmd=`cat $1/configuration | grep mount_cmd | awk '{print $3}'`
mount_file=`cat $1/configuration | grep mount_file | awk '{print $3}'`
sync_file=`cat $1/configuration | grep sync_file | awk '{print $3}'`

check_stat=`ps aux | grep 'rsync -avh' |  grep api-data | wc -l`
if [ $check_stat -eq "0" ];then
  check_mount_stat=`mount |  grep $mount_file | wc -l`
  if [ $check_mount_stat -eq "0" ];then
    mount $mount_cmd $mount_file
  fi
  rsync -avh --delete $mount_file/api-data/ $sync_file
  umount $mount_file
fi

