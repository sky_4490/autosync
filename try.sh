#/bin/bash
mount_cmd=`cat $1/configuration | grep mount_cmd | awk '{print $3}'`
mount_file=`cat $1/configuration | grep mount_file | awk '{print $3}'`
device_id=`cat $1/configuration | grep device_id | awk '{print $3}'`


check_mount_stat=`mount | grep $mount_file | wc -l`
if [ $check_mount_stat -eq "0" ]; then
   mount $mount_cmd $mount_file
fi

cd $mount_file
if [ -f "tymetro-roid-$device_id.zip" ]; then
  unzip -o tymetro-roid-$device_id.zip  -d $unzip_dir

umount $mount_file
