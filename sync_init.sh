#!/bin/bash

if [ "$#" -ne 1 ]; then
  echo "Usage: $0 <autosync_directory>"
  exit 1
fi

listen_port=`cat $1/configuration | grep listen_port | awk '{print $3}'`

if [[ -n "$listen_port" ]]; then
  if [[ $listen_port -ge 1 ]] && [[ $listen_port -le 65535 ]]; then
    echo "use port: $listen_port for service.";
  else
    echo "listen_port out of range, use default port 55555";
    listen_port=55555
fi
else
   echo "the port was not set, use default port 55555"
   listen_port=55555
fi


if ! screen -list | grep -q "manual_sync"; then
  screen -S manual_sync -dm ncat -4lk $listen_port --delay 5 --sh-exec "$1/sync_v1.0.sh $1"
fi

screen -S rync -dm  $1/sync_v1.0.sh $1
